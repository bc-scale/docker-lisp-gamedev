docker-lisp-gamedev
-------------------
A Docker image containing tools necessary for Common Lisp game development and deployment:
* [SBCL](http://sbcl.org)
* [Quicklisp](https://quicklisp.org)
* [libffi](https://sourceware.org/libffi)
* [GCC](https://gcc.gnu.org)
* [Git](https://git-scm.com)
* [Git LFS](https://git-lfs.github.com)
* [liballegro](https://liballeg.org)
* [appimagetool](https://appimage.github.io/appimagetool) & [linuxdeploy](https://github.com/linuxdeploy/linuxdeploy)
* [rlwrap](https://github.com/hanslub42/rlwrap) :blush:

The image is based on Ubuntu 16.04 so that resulting AppImages support widest range of possible Linux distributions with old Glibc versions.

Usage
-----
```sh
$ docker run -it -v `pwd`:/opt/game lockie/docker-lisp-gamedev /bin/bash
# #start hacking
```

Contributing
------------
Feel free to request new software to be added via [issue tracker](https://gitlab.com/lockie/docker-lisp-gamedev/-/issues/new).

Merge requests are welcome. For major changes, please [open an issue](https://gitlab.com/lockie/docker-lisp-gamedev/-/issues/new) first to discuss what you would like to change.

License
-------
[2-clause BSD](https://choosealicense.com/licenses/bsd-2-clause).

This project is heavily based on [cl-docker-images](https://gitlab.common-lisp.net/cl-docker-images/sbcl), Copyright (c) 2018-2020 by Eric Timmons.
